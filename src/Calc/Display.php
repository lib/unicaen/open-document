<?php

namespace Unicaen\OpenDocument\Calc;

use Unicaen\OpenDocument\Calc;

class Display
{

    static public function sheet(Sheet $sheet): string
    {
        $h = '<table class="table table-bordered table-condensed">';
        $h .= '<tr><th></th>';
        for ($c = 1; $c <= $sheet->getMaxCol(); $c++) {
            $h .= '<th>' . (Calc::numberToLetter($c)) . '</th>';
        }
        $h .= '</tr>';
        for ($r = 1; $r <= $sheet->getMaxRow(); $r++) {
            $h .= '<tr><th>' . $r . '</th>';
            for ($c = 1; $c <= $sheet->getMaxCol(); $c++) {
                $cell = $sheet->getCellByCoords($c, $r);
                if ($cell && $cell->getRow() == $r && $cell->getCol() == $c) {
                    $h .= '<td rowspan="' . $cell->getRowSpan() . '" colspan="' . $cell->getColSpan() . '">' . ($cell ? $cell->getValue() : '') . '</td>';
                } elseif (!$cell) {
                    $h .= '<td></td>';
                }
            }
            $h .= '</tr>';
        }

        $h .= '</table>';

        return $h;
    }



    static public function formule(Formule $formule)
    {
        $h = '';
        $h .= '<div style="clear: both"><code>' . htmlentities($formule->text()) . '</code></div>';

        $expr = $formule->analyse();

        $h .= self::formuleExpr($expr);

        return $h;
    }



    static public function formuleExpr(array $expr, $inFunction=false): string
    {
        $h = '';

        if (isset($expr['type'])){

            // sous-expression
            $h .= '        <table class="table table-bordered table-condensed table-xs" style="width:auto">';
            foreach ($expr as $t => $v) {
                if ($t == 'level'){
                    continue;
                }
                if (is_array($v)){
                    $v = self::formuleExpr($v, $expr['type'] == 'function');
                }else{
                    $v = htmlentities($v);
                }

                $h .= '        <tr>';
                $h .= '            <th>' . $t . '</th>';
                $h .= '            <td>' . $v . '</td>';
                $h .= '        </tr>';
            }
            $h .= '        </table>';
        }else{
            // liste de sous-expressions
            $h .= '<table class="table-xs"><tr>';
            $first = true;
            foreach ($expr as $index => $term) {
                if ($inFunction && !$first){
                    $h .= '<td style="vertical-align: top">&nbsp;;&nbsp;</td>';
                }
                $h .= '<td style="vertical-align: top">'.self::formuleExpr($term).'</td>';
                $first = false;
            }
            $h .= '</tr></table>';
        }

        return $h;
    }
}