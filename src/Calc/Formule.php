<?php

namespace Unicaen\OpenDocument\Calc;

use Unicaen\OpenDocument\Calc;

class Formule
{
    private string $formule;

    private array $terms = [];

    private array $expr = [];



    public function __construct(string $formule)
    {
        $this->formule = $formule;
    }



    private function t(int $index): array
    {
        if (array_key_exists($index, $this->terms)) {
            return $this->terms[$index];
        } else {
            return ['type' => 'end', 'v' => null];
        }
    }



    private function tw(array $term, int $index, int $length = 1)
    {
        $this->terms[$index] = $term;
        if ($length > 1) {
            for ($i = 1; $i < $length; $i++) {
                unset($this->terms[$index + $i]);
            }
            $this->terms = array_values($this->terms);
        }
    }



    public function text(): string
    {
        return $this->formule;
    }



    public function analyse(): array
    {
        if (empty($this->terms)) {

            $start = 0;
            if (str_starts_with($this->formule, 'of:=')) {
                $start = 4;
            }
            if (str_starts_with($this->formule, 'of:==')) {
                $start = 5;
            }

            for ($i = $start; $i < strlen($this->formule); $i++) {
                $this->terms[] = [
                    'type' => null,
                    'v'    => $this->formule[$i],
                ];
            }

            $this->lexing();
            $this->grammar();
        }

        return $this->expr;
    }



    public function getDeps(): array
    {
        $deps = [];

        $this->analyse();

        foreach ($this->terms as $term) {
            switch($term['type']){
                case 'cell':
                    $deps[] = $term['name'];
                    break;
                case 'variable':
                    $deps[] = $term;
                    break;
                case 'range':
                    $deps[] = $term;
                    break;
            }
        }

        return $deps;
    }



    private function lexing()
    {
        $lexers = [
            'lexString',
            'lexVarRange',
            'lexOpsSeps',
            'lexOthers',
        ];

        foreach ($lexers as $lexer) {
            $i = 0;
            while (($t = $this->t($i)) && $t['type'] !== 'end') {
                if ($t['type'] === null) {
                    $this->{$lexer}($i);
                }
                $i++;
            }
        }
    }



    private function lexString(int $i)
    {
        $t = $this->t($i);
        if ($t['v'] !== '"') {
            return;
        }

        $string = '';
        $length = 2;

        $c = $i;
        do {
            $c++;
            $t = $this->t($c);
            if ($t['type'] === null) {
                if ($t['v'] !== '"') {
                    $string .= $t['v'];
                    $length++;
                } else {
                    $next = $this->t($c + 1);
                    if ($next['type'] === null && $next['v'] === '"') {
                        $string .= '"';
                        $length += 2;
                        $c++;
                        $t['v'] = '';
                    }
                }
            }
        } while (!($t['type'] != null || $t['v'] === '"'));

        $this->tw([
            'type'    => 'string',
            'content' => $string,
        ], $i, $length);
    }



    private function lexVarRange(int $i)
    {
        $t = $this->t($i);
        if ($t['v'] !== '[') {
            return;
        }

        $val = '';
        $length = 1;

        $c = $i;
        do {
            $c++;
            $t = $this->t($c);
            if ($t['v'] != ']') {
                $val .= $t['v'];
            }
            $length++;
        } while ($t['type'] === null && $t['v'] !== ']');

        if (false !== strpos($val, ':')) {
            $range = explode(':', $val);
            [$sheet, $begin] = explode('.', $range[0]);
            $end = substr($range[1], 1);

            if (str_starts_with($sheet, '$')){
                $sheet = substr($sheet,1);
            }

            if (str_starts_with($sheet, "'")){
                $sheet = substr($sheet,1,-1);
            }

            $cBegin = Calc::cellNameToCoords($begin);
            $cEnd = Calc::cellNameToCoords($end);

            $term = [
                'type'          => 'range',
                'begin'         => $begin,
                'end'           => $end,
                'rowBegin'      => $cBegin['row'],
                'rowEnd'        => $cEnd['row'],
                'colBegin'      => $cBegin['col'],
                'colNamedBegin' => Calc::numberToLetter($cBegin['col']),
                'colEnd'        => $cEnd['col'],
                'colNamedEnd'   => Calc::numberToLetter($cEnd['col']),
            ];
            if ($sheet){
                $term['sheet'] = $sheet;
            }
        } else {
            [$sheet, $name] = explode('.', $val);

            if (str_starts_with($sheet, '$')){
                $sheet = substr($sheet,1);
            }

            if (str_starts_with($sheet, "'")){
                $sheet = substr($sheet,1,-1);
            }

            $term = [
                'type' => 'cell',
                'name' => $name,
            ];
            if ($sheet){
                $term['sheet'] = $sheet;
            }
        }

        $this->tw($term, $i, $length);
    }



    private function lexOpsSeps(int $i)
    {
        $t = $this->t($i);

        if ($t['v'] === '<') {
            $next = $this->t($i + 1);
            if ($next['type'] === null && $next['v'] === '>') {
                $this->tw(['type' => 'op', 'name' => '<>'], $i, 2);

                return;
            }
            if ($next['type'] === null && $next['v'] === '=') {
                $this->tw(['type' => 'op', 'name' => '<='], $i, 2);

                return;
            }
        } elseif ($t['v'] === '>') {
            $next = $this->t($i + 1);
            if ($next['type'] === null && $next['v'] === '=') {
                $this->tw(['type' => 'op', 'name' => '>='], $i, 2);

                return;
            }
        }

        $ops = ['<', '>', '-', '+', '*', '/', '=', '&', '%'];
        if (in_array($t['v'], $ops)) {
            $this->tw(['type' => 'op', 'name' => $t['v']], $i);
        }

        $seps = ['(', ')', ';'];
        if (in_array($t['v'], $seps)) {
            $this->tw(['type' => 'sep', 'name' => $t['v']], $i);
        }
    }



    private function lexOthers(int $i)
    {
        $c = $i;
        $t = $this->t($c);
        $v = '';
        $length = 0;
        $type = null;
        while ($t['type'] === null && $t['type'] !== 'end') {

            $v .= $t['v'];
            $length++;
            $c++;
            $t = $this->t($c);
        }

        /* Détection de nombres */
        if ($v === '0') {
            $this->tw(['type' => 'number', 'value' => 0], $i, $length);

            return;
        } else {
            $vt = str_replace([',', ' '], ['.', ''], $v);
            $vf = (float)$vt;
            if ((string)$vf === $vt) {
                $this->tw(['type' => 'number', 'value' => $vf], $i, $length);

                return;
            }
        }

        if ('' == trim($v)){
            $this->tw(['type' => 'space', 'name' => $v], $i, $length);
        }else{
            $this->tw(['type' => 'variable', 'name' => trim($v)], $i, $length);
        }


        /* Détection de fonctions */
        $next = $this->t($i + 1);
        if ($next['type'] === 'sep' && $next['name'] === '(') {
            $this->tw(['type' => 'function', 'name' => trim($v)], $i);
        }
    }



    private function grammar()
    {
        $expr = &$this->terms;


        // on met la structure en relief
        $level = 1;
        foreach ($expr as $i => $term) {
            if ($term['type'] === 'sep' && $term['name'] === ')') {
                $level--;
            }
            if ($term['type'] === 'sep' && $term['name'] === ';') {
                $expr[$i]['level'] = $level - 1;
            } else {
                $expr[$i]['level'] = $level;
            }

            if ($term['type'] === 'sep' && $term['name'] === '(') {
                $level++;
            }
        }

        $this->expr = $this->terms;

        $this->analyseExpr($this->expr, 0);
    }



    private function searchExpr(array &$expr, int $i, array $filters): ?int
    {
        foreach ($expr as $c => $term) {
            if ($c >= $i) {
                $match = true;
                foreach ($filters as $name => $value) {
                    if (!isset($term[$name]) || $term[$name] !== $value) {
                        $match = false;
                    }
                    if ($match) return $c;
                }
            }
        }

        return null;
    }



    private function analyseExpr(array &$expr, int $i)
    {
        $c = $i;
        while (($c = $this->searchExpr($expr, $c, ['type' => 'function'])) !== null) {
            $this->buildFunction($expr, $c);
            $c++;
        }

        $c = $i;
        while (($c = $this->searchExpr($expr, $c, ['type' => 'sep', 'name' => '('])) !== null) {
            $this->buildExpr($expr, $c);
            $c++;
        }
    }



    private function buildFunction(array &$expr, int $i)
    {
        $level = $expr[$i]['level'];
        $exprs = [];
        $currentExpr = 0;
        $end = count($expr);

        if (!(isset($expr[$i + 1]) && $expr[$i + 1]['type'] === 'sep' && $expr[$i + 1]['name'] === '(')) {
            // Fonction déjà analysée
            return;
        }

        unset($expr[$i + 1]); // on supprime la parenthèse ouvrante
        for ($c = $i + 2; $c < $end; $c++) {
            $t = $expr[$c];
            unset($expr[$c]);
            if ($t['type'] === 'sep' && $t['name'] === ')' && $t['level'] === $level) {
                // fin de la fonction
                break;
            } elseif ($t['type'] === 'sep' && $t['name'] === ';' && $t['level'] === $level) {
                $currentExpr++;
            } else {
                $exprs[$currentExpr][] = $t;
            }
        }

        for ($e = 0; $e <= $currentExpr; $e++) {
            $this->analyseExpr($exprs[$e], 0);
        }
        $expr[$i]['exprs'] = $exprs;
        $expr = array_values($expr);
    }



    private function buildExpr(array &$expr, int $i)
    {
        $level = $expr[$i]['level'];
        $sousExpr = [];
        $end = count($expr);

        for ($c = $i + 1; $c < $end; $c++) {
            $t = $expr[$c];
            unset($expr[$c]);
            if ($t['type'] === 'sep' && $t['name'] === ')' && $t['level'] === $level) {
                // fin de la fonction
                break;
            } else {
                $sousExpr[] = $t;
            }
        }

        $this->analyseExpr($sousExpr, 0);
        $expr[$i] = [
            'type' => 'expr',
            'expr' => $sousExpr,
        ];
        $expr = array_values($expr);
    }

}