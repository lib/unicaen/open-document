<?php

namespace Unicaen\OpenDocument\Calc;


use Unicaen\OpenDocument\Calc;
use DOMNode;


class Sheet
{
    private Calc    $calc;

    private int     $index;

    private DOMNode $node;

    /**
     * @var Cell[][]
     */
    private array $cells = [];

    /**
     * @var Cell[]
     */
    private array $cellsByName = [];

    private int   $maxCol      = 0;

    private int   $maxRow      = 0;



    public function __construct(Calc $calc, int $index, DOMNode $node)
    {
        $this->calc  = $calc;
        $this->index = $index;
        $this->node  = $node;
        $this->read();
    }



    /**
     * @return DOMNode
     */
    public function getNode(): DOMNode
    {
        return $this->node;
    }



    public function getIndex(): int
    {
        return $this->index;
    }



    public function getName(): string
    {
        return $this->node->getAttribute('table:name');
    }



    public function read()
    {
        $this->cells       = [];
        $this->cellsByName = [];
        $this->maxRow      = 0;
        $this->maxCol      = 0;
        $rowIndex          = 0;

        /** @var DOMNode[] $rcs */
        $rcs               = $this->node->childNodes;
        foreach ($rcs as $rc) {
            switch ($rc->tagName) {
                case 'office:forms':
                break;
                case 'table:table-column':
                break;
                case 'table:table-row':
                    $rowSpan = $rc->attributes->getNamedItem('number-rows-repeated')?->value ?: 1;
                    $this->readRow($rc, $rowIndex += $rowSpan);
                break;
            }
        }
    }



    protected function readRow(DOMNode $rowNode, int $row)
    {
        $cells = $rowNode->childNodes;
        $col   = 1;

        //echo '<h2>ROW ' . $row . '</h2>';
        //xmlDump($rowNode);
        foreach ($cells as $c) {
            if ('table:covered-table-cell' == $c->tagName) {
                $covered = (int)$c->attributes->getNamedItem('number-columns-repeated')?->value ?: 1;
                $col += $covered;
            }
            if ('table:table-cell' == $c->tagName) {
                $cell = new Cell($c);
                if ($repeated = $cell->getRepeated()) {
                    $col += $repeated;
                    continue;
                }

                $colSpan = $cell->getColSpan();
                $rowSpan = $cell->getRowSpan();

                $cell->setCol($col);
                $cell->setRow($row);
                for ($c = 0; $c < $colSpan; $c++) {
                    for ($r = 0; $r < $rowSpan; $r++) {
                        $this->addCell($cell, $col+$c, $row+$r);
                    }
                }

                //var_dump([
                //    'col'     => $col,
                //    'value'   => $cell->getContent(),
                //    'colSpan' => $colSpan,
                //    'rowSpan' => $rowSpan,
                //]);

                $col ++;
            }
        }
    }



    private function addCell(Cell $cell, int|string $col, int $row): void
    {
        if (is_string($col)){
            $col = Calc::letterToNumber($col);
        }
        if (!isset($this->cells[$col])) {
            $this->cells[$col] = [];
        }
        $this->cells[$col][$row] = $cell;

        if ($row > $this->maxRow) $this->maxRow = $row;
        if ($col > $this->maxCol) $this->maxCol = $col;

        $name = Calc::coordsToCellName($col, $row);

        $this->cellsByName[$name] = $cell;
    }



    public function getCells(): array
    {
        return $this->cells;
    }



    public function getCellsByNames(): array
    {
        return $this->cellsByName;
    }



    public function getRefCells(Cell $cell, $recursive = false): array
    {
        $refs = [];

        $formule = $cell->getFormule();
        $formule = str_replace('$', '', $formule);

        foreach ($this->cellsByName as $name => $cell) {
            if (false !== strpos($formule, "[.$name]")) {
                $refs[$name] = $cell;
            }
        }

        if (true === $recursive) {
            foreach ($refs as $ref) {
                $rRefs = $this->getRefCells($ref, true);
                foreach ($rRefs as $rn => $rc) {
                    $refs[$rn] = $rc;
                }
            }
        }

        return $refs;
    }



    public function getMaxCol(): int
    {
        return $this->maxCol;
    }



    public function getMaxRow(): int
    {
        return $this->maxRow;
    }



    public function hasCellByCoords(int|string $col, int $row): bool
    {
        if (is_string($col)){
            $col = Calc::letterToNumber($col);
        }
        return array_key_exists($col, $this->cells) && array_key_exists($row, $this->cells[$col]) && $this->cells[$col][$row] instanceof Cell;
    }



    public function getCellByCoords(int|string $col, int $row): ?Cell
    {
        if (is_string($col)){
            $col = Calc::letterToNumber($col);
        }
        if ($this->hasCellByCoords($col, $row)) {
            return $this->cells[$col][$row];
        }

        return null;
    }



    public function hasCell(string $name): bool
    {
        ['col' => $col, 'row' => $row] = Calc::cellNameToCoords($name);

        return $this->hasCellByCoords($col, $row);
    }



    public function getCell(string $name): ?Cell
    {
        ['col' => $col, 'row' => $row] = Calc::cellNameToCoords($name);

        return $this->getCellByCoords($col, $row);
    }

}