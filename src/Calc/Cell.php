<?php

namespace Unicaen\OpenDocument\Calc;

use Unicaen\OpenDocument\Calc;
use DOMNode;

class Cell
{
    private DOMNode $node;

    private int $row = 1;

    private int $col = 1;



    /**
     * @inheritDoc
     */
    public function __construct(DOMNode $node)
    {
        $this->node = $node;
    }



    /**
     * @return DOMNode
     */
    public function getNode(): DOMNode
    {
        return $this->node;
    }



    /**
     * @return int
     */
    public function getRow(): int
    {
        return $this->row;
    }



    /**
     * @param int $row
     *
     * @return Cell
     */
    public function setRow(int $row): Cell
    {
        $this->row = $row;

        return $this;
    }



    /**
     * @return int
     */
    public function getCol(): int
    {
        return $this->col;
    }



    /**
     * @param int $col
     *
     * @return Cell
     */
    public function setCol(int $col): Cell
    {
        $this->col = $col;

        return $this;
    }



    /**
     * @return string
     */
    public function getType(): string
    {
        if ($this->node->hasAttributes()) {
            $type = $this->node->attributes->getNamedItem('value-type');
            if ($type) return $type->value;
        }
    }



    /**
     * @return string
     */
    public function getContent(): string
    {
        $childs = $this->node->childNodes;
        $content = '';
        foreach ($childs as $child) {
            if ($child->tagName === 'text:p') {
                if ($content != ''){
                    $content .= "\n";
                }
                $content .= $child->textContent;
            }
        }

        return $content;
    }



    /**
     * @return string
     */
    public function getFormule(): ?string
    {
        if ($this->node->hasAttributes()) {
            $formule = $this->node->attributes->getNamedItem('formula');
            if ($formule) return $formule->value;
        }

        return null;
    }



    public function getFormuleExpr(): ?array
    {
        $fText = $this->getFormule();
        if ($fText === null) $fText = $this->getValue();
        if ($fText === null) return null;

        $formule = new Formule($fText);

        return $formule->analyse();
    }



    public function getDeps(): array
    {
        $fText = $this->getFormule();
        if (!$fText) return [];

        $formule = new Formule($fText);

        return $formule->getDeps();
    }



    /**
     * @return mixed
     */
    public function getValue()
    {
        if (!$this->node->nodeValue) {
            return null;
        }
        if ($this->node->hasAttributes()) {
            $value = $this->node->attributes->getNamedItem('value');
            if ($value) return $value->value;
            $value = $this->node->nodeValue;
            return $value;
        }

        return null;
    }



    /**
     * @return int
     */
    public function getColSpan(): int
    {
        if ($this->node->hasAttributes()) {
            $colSpan = $this->node->attributes->getNamedItem('number-columns-spanned');
            if ($colSpan) return (int)$colSpan->value;
        }

        return 1;
    }



    public function getRepeated(): ?int
    {
        if ($this->node->hasAttributes()) {
            $colSpan = $this->node->attributes->getNamedItem('number-columns-repeated');
            if ($colSpan) return (int)$colSpan->value;
        }

        return null;
    }



    /**
     * @return int
     */
    public function getRowSpan(): int
    {
        if ($this->node->hasAttributes()) {
            $rowSpan = $this->node->attributes->getNamedItem('number-rows-spanned');
            if ($rowSpan) return (int)$rowSpan->value;
        }

        return 1;
    }



    public function getName()
    {
        return Calc::numberToLetter($this->getCol()) . $this->getRow();
    }

}