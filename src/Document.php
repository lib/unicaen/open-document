<?php

namespace Unicaen\OpenDocument;

use DOMDocument;
use DOMElement;
use DOMNode;
use DOMNodeList;
use Exception;
use ZipArchive;


class Document
{

    /**
     * @var ZipArchive
     */
    private $archive;

    /**
     * @var bool
     */
    private $pdfOutput = false;

    /**
     * @var Publisher
     */
    private $publisher;

    /**
     * @var Calc
     */
    private $calc;
    
    /**
     * @var Stylist
     */
    private $stylist;

    /**
     * @var DomDocument;
     */
    private $meta;

    /**
     * @var DOMDocument
     */
    private $styles;

    /**
     * @var DOMDocument
     */
    private $content;

    /**
     * @var array
     */
    private $namespaces = [];

    /**
     * @var bool
     */
    private $metaChanged = false;

    /**
     * @var bool
     */
    private $stylesChanged = false;

    /**
     * @var bool
     */
    private $contentChanged = false;

    /**
     * @var string
     */
    private $convCommand = 'unoconv --server=:host -f :format -o :outputFile :inputFile';

    /**
     * @var string
     */
    private $tmpDir;

    /**
     * @var string
     */
    private $host = '127.0.0.1';

    /**
     * @var array
     */
    private $formatters = [];


    /**
     * @var array
     */
    private $tmpFiles          = [];

    private $defaultNamespaces = [
        'office'    => "urn:oasis:names:tc:opendocument:xmlns:office:1.0",
        'style'     => "urn:oasis:names:tc:opendocument:xmlns:style:1.0",
        'text'      => "urn:oasis:names:tc:opendocument:xmlns:text:1.0",
        'table'     => "urn:oasis:names:tc:opendocument:xmlns:table:1.0",
        'draw'      => "urn:oasis:names:tc:opendocument:xmlns:drawing:1.0",
        'fo'        => "urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0",
        'xlink'     => "http://www.w3.org/1999/xlink",
        'dc'        => "http://purl.org/dc/elements/1.1/",
        'meta'      => "urn:oasis:names:tc:opendocument:xmlns:meta:1.0",
        'number'    => "urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0",
        'svg'       => "urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0",
        'chart'     => "urn:oasis:names:tc:opendocument:xmlns:chart:1.0",
        'dr3d'      => "urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0",
        'math'      => "http://www.w3.org/1998/Math/MathML",
        'form'      => "urn:oasis:names:tc:opendocument:xmlns:form:1.0",
        'script'    => "urn:oasis:names:tc:opendocument:xmlns:script:1.0",
        'ooo'       => "http://openoffice.org/2004/office",
        'ooow'      => "http://openoffice.org/2004/writer",
        'oooc'      => "http://openoffice.org/2004/calc",
        'dom'       => "http://www.w3.org/2001/xml-events",
        'rpt'       => "http://openoffice.org/2005/report",
        'of'        => "urn:oasis:names:tc:opendocument:xmlns:of:1.2",
        'xhtml'     => "http://www.w3.org/1999/xhtml",
        'grddl'     => "http://www.w3.org/2003/g/data-view#",
        'officeooo' => "http://openoffice.org/2009/office",
        'tableooo'  => "http://openoffice.org/2009/table",
        'drawooo'   => "http://openoffice.org/2010/draw",
        'calcext'   => "urn:org:documentfoundation:names:experimental:calc:xmlns:calcext:1.0",
        'css3t'     => "http://www.w3.org/TR/css3-text/",
        'xforms'    => 'http://www.w3.org/2002/xforms',
        'xsd'       => 'http://www.w3.org/2001/XMLSchema',
        'xsi'       => 'http://www.w3.org/2001/XMLSchema-instance',
        'loext'     => 'urn:org:documentfoundation:names:experimental:office:xmlns:loext:1.0',
        'field'     => 'urn:openoffice:names:experimental:ooo-ms-interop:xmlns:field:1.0',
        'formx'     => 'urn:openoffice:names:experimental:ooxml-odf-interop:xmlns:form:1.0',
    ];



    /**
     * @return ZipArchive
     * @throws Exception
     */
    public function getArchive(): ZipArchive
    {
        if (!$this->archive) {
            throw new \Exception("Aucun document n\'est chargé");
        }

        return $this->archive;
    }



    /**
     * @return DOMDocument
     */
    public function getContent(): DOMDocument
    {
        return $this->content;
    }



    /**
     * @return DOMDocument
     */
    public function getStyles(): DOMDocument
    {
        return $this->styles;
    }



    /**
     * @return DOMDocument
     */
    public function getMeta(): DOMDocument
    {
        return $this->meta;
    }



    /**
     * @return bool
     */
    public function isPdfOutput(): bool
    {
        return $this->pdfOutput;
    }



    /**
     * @param bool $pdfOutput
     *
     * @return Document
     */
    public function setPdfOutput(bool $pdfOutput): Document
    {
        $this->pdfOutput = $pdfOutput;

        return $this;
    }



    /**
     * @return bool
     */
    public function isMetaChanged(): bool
    {
        return $this->metaChanged;
    }



    /**
     * @param bool $metaChanged
     *
     * @return Document
     */
    public function setMetaChanged(bool $metaChanged): Document
    {
        $this->metaChanged = $metaChanged;

        return $this;
    }



    /**
     * @return bool
     */
    public function isStylesChanged(): bool
    {
        return $this->stylesChanged;
    }



    /**
     * @param bool $stylesChanged
     *
     * @return Document
     */
    public function setStylesChanged(bool $stylesChanged): Document
    {
        $this->stylesChanged = $stylesChanged;

        return $this;
    }



    /**
     * @return bool
     */
    public function isContentChanged(): bool
    {
        return $this->contentChanged;
    }



    /**
     * @param bool $contentChanged
     *
     * @return Document
     */
    public function setContentChanged(bool $contentChanged): Document
    {
        $this->contentChanged = $contentChanged;

        return $this;
    }



    /**
     * @return string
     */
    public function getConvCommand(): string
    {
        return $this->convCommand;
    }



    /**
     * @param string $convCommand
     *
     * @return Document
     */
    public function setConvCommand(string $convCommand): Document
    {
        $this->convCommand = $convCommand;

        return $this;
    }



    /**
     * Retourne les méta-données du fichier OpenDocument
     *
     * @return array
     */
    public function getMetaArray(): array
    {
        $m = [];

        $nodes = $this->getMeta()->documentElement->childNodes->item(0)->childNodes;
        foreach ($nodes as $node) {
            if (isset($node->tagName)) {
                switch ($node->tagName) {
                    case 'meta:generator':
                    case 'meta:initial-creator':
                    case 'meta:editing-cycles':
                    case 'meta:editing-duration':
                    case 'meta:printed-by':
                    case 'dc:title':
                    case 'dc:description':
                    case 'dc:subject':
                    case 'dc:creator':
                    case 'dc:language':
                        list($ns, $tag) = explode(':', $node->tagName);
                        $m[$tag] = $node->textContent;
                    break;
                    case 'meta:creation-date':
                        $m['creation-date'] = substr($node->textContent, 0, 10);
                    break;
                    case 'meta:print_date':
                        $m['print_date'] = substr($node->textContent, 0, 10);
                    break;
                    case 'dc:date':
                        $m['date'] = substr($node->textContent, 0, 10);
                    break;
                    case 'meta:document-statistic':
                        $m['document-statistic'] = [];
                        foreach ($node->attributes as $attribute) {
                            $m['document-statistic'][$attribute->name] = $attribute->value;
                        }
                    break;
                    case 'meta:user-defined':
                        if (!isset($m['user-defined'])) $m['user-defined'] = [];
                        foreach ($node->attributes as $attribute) {
                            $m['user-defined'][$attribute->name] = $attribute->value;
                        }
                    break;
                    case 'meta:keywords':
                        $m['keywords'] = [];
                        foreach ($node->childNodes as $knode) {
                            $m['keywords'][] = $knode->textContent;
                        }
                    break;
                }
            }
        }

        return $m;
    }



    /**
     * @param string $variable
     * @param mixed $value
     *
     * @return string
     */
    public function formatValue(string $variable, $value): string
    {
        if (isset($this->formatters[$variable]) && $format = $this->formatters[$variable]){
            if (is_callable($format)){
                $value = $format($value);
            }
        }

        if (is_float($value)){
            return number_format($value, 2, ',', ' ');
        }

        return (string)$value;
    }



    /**
     * Retourne les espaces de nom associés à leurs URI respectives sous forme de tableau associatif
     *
     * @return array
     */
    public function getNamespaces(): array
    {
        if (empty($this->namespaces)) {
            $content = $this->getArchive()->getFromName('content.xml');

            $begin            = strpos($content, '<', 1) + 2 + strlen('office:document-content');
            $end              = strpos($content, '>', 50) - $begin;
            $content          = explode(' ', substr($content, $begin, $end));
            $this->namespaces = $this->defaultNamespaces;
            foreach ($content as $str) {
                if (0 === strpos($str, 'xmlns:')) {
                    $namespace                    = substr($str, 6, strpos($str, '"') - 7);
                    $url                          = substr($str, strpos($str, '"') + 1, -1);
                    $this->namespaces[$namespace] = $url;
                }
            }
        }

        return $this->namespaces;
    }



    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasNamespace(string $name): bool
    {
        $this->getNamespaces();

        return isset($this->namespaces[$name]);
    }



    /**
     * @param string $name
     * @param string $url
     *
     * @return Document
     */
    public function addNamespace(string $name, string $url): Document
    {
        if (!$this->hasNamespace($name)) {
            $this->namespaces[$name] = $url;
        }

        return $this;
    }



    /**
     * Retourne l'url associé à un espace de nom
     *
     * @param string $namespace
     *
     * @return string
     */
    public function getNamespaceUrl(string $namespace): string
    {
        $ns = $this->getNamespaces();

        if (!isset($ns[$namespace])) {
            throw new Exception('L\'espace de nom ' . $namespace . ' n\'a pas été trouvé.');
        }

        return $ns[$namespace];
    }



    /**
     * Retourne un champ d'information
     *
     * @param integer $index
     *
     * @return string
     */
    public function getInfo($index)
    {
        $infonodes = $this->getMeta()->getElementsByTagNameNS($this->getNamespaceUrl('meta'), 'user-defined');

        return $infonodes->item($index)->nodeValue;
    }



    /**
     * Modifie un champ d'information
     *
     * @todo à finir d'implémenter, car ça ne marche pas!!
     *
     * @param integer $index
     * @param string  $value
     */
    public function setInfo($index, $value): Document
    {
        throw new \Exception('Implémentation à corriger : ne fonctionne pas');

        $infonodes = $this->getMeta()->getElementsByTagNameNS($this->getNamespaceUrl('meta'), 'user-defined');
        if ($infonodes->length > 0) {
            $infonodes->item($index)->nodeValue = $value;
            $this->setMetaChanged(true);
        }

        return $this;
    }



    /**
     * @param array $values
     *
     * @return Document
     */
    public function publish(array $values): Document
    {
        $this->getPublisher()->setValues($values);
        $this->getPublisher()->publish();

        return $this;
    }



    /**
     * @return Publisher
     */
    public function getPublisher(): Publisher
    {
        if (!$this->publisher) {
            $this->publisher = new Publisher();
            $this->publisher->setDocument($this);
        }

        return $this->publisher;
    }



    /**
     * @return Publisher
     */
    public function getCalc(): Calc
    {
        if (!$this->calc) {
            $this->calc = new Calc($this);
        }

        return $this->calc;
    }
    


    /**
     * @return Stylist
     */
    public function getStylist(): Stylist
    {
        if (!$this->stylist) {
            $this->stylist = new Stylist();
            $this->stylist->setDocument($this);
        }

        return $this->stylist;
    }



    /**
     * @param $data
     *
     * @return Document
     * @throws Exception
     */
    public function loadFromData($data): Document
    {
        if (!class_exists('ZipArchive')) {
            throw new Exception('Zip extension not loaded');
        }

        if (!$data){
            throw new Exception('Wrong or empty data');
        }

        $odtFile = $this->tempFileName('odtfile_', 'odt');
        file_put_contents($odtFile, $data);

        return $this->loadFromFile($odtFile, false);
    }



    /**
     * @param string $fileName
     * @param bool   $duplicate
     *
     * @return Document
     * @throws Exception
     */
    public function loadFromFile(string $fileName, bool $duplicate = true): Document
    {
        if (!class_exists('ZipArchive')) {
            throw new Exception('Zip extension not loaded');
        }

        if (!file_exists($fileName)) {
            throw new Exception('OpenDocument file "' . $fileName . '" doesn\'t exists.');
        }

        $mimetype = mime_content_type($fileName);
        /* autoconvertion en OpenDocument */
        if (str_contains($mimetype, 'openxmlformats') && str_contains($mimetype, 'spreadsheetml')){
            $fileName = $this->convert($fileName, 'ods');
            $duplicate = false;
        }
        if (str_contains($mimetype, 'openxmlformats') && str_contains($mimetype, 'wordprocessingml')){
            $fileName = $this->convert($fileName, 'odt');
            $duplicate = false;
        }

        if ($duplicate) {
            $odtFile = $this->tempFileName('odtFile_', 'odt');
            copy($fileName, $odtFile);
        } else {
            $odtFile = $fileName;
        }

        $this->archive = new ZipArchive();
        if (true !== $this->archive->open($odtFile, ZIPARCHIVE::CREATE)) {
            throw new Exception('OpenDocument file "' . $fileName . '" don\'t readable.');
        }

        $this->meta = new DOMDocument;
        $this->meta->loadXML($this->archive->getFromName('meta.xml'));

        $this->styles = new DOMDocument;
        $this->styles->loadXML($this->archive->getFromName('styles.xml'));

        $this->content = new DOMDocument;
        $this->content->loadXML($this->archive->getFromName('content.xml'));

        $this->namespaces = [];

        return $this;
    }



    /**
     * @return string
     * @throws Exception
     */
    private function prepareSaving($filename = null): string
    {
        $actualFile = $this->getArchive()->filename;
        if (!is_writable($actualFile)) {
            throw new Exception('Impossible to create or update the document. Directory "'.dirname($actualFile).'" not reachable or writable');
        }

        if ($this->isMetaChanged()) {
            $this->getArchive()->addFromString('meta.xml', $this->getMeta()->saveXML());
        }

        if ($this->isStylesChanged()) {
            $this->getArchive()->addFromString('styles.xml', $this->getStyles()->saveXML());
        }

        if ($this->isContentChanged()) {
            $this->getArchive()->addFromString('content.xml', $this->getContent()->saveXML());
        }

        $this->getArchive()->close();
        $this->archive = null;

        if ($this->isPdfOutput()) {
            if (!$filename) {
                $filename = $this->tempFileName('odt2pdf_', 'pdf');
            }
            $this->toPdf($actualFile, $filename);
            $actualFile = $filename;
        }else{
            if ($filename && $filename != $actualFile){
                if (!is_writable(dirname($filename))) {
                    throw new Exception('Impossible to create or update the document. Directory "'.dirname($filename).'" not reachable or writable');
                }
                copy($actualFile, $filename);
            }
        }

        return $actualFile;
    }



    /**
     * @param $origine
     * @param $destination
     *
     * @return string
     * @throws Exception
     */
    public function toPdf(string $origine, string $destination=null): string
    {
        return $this->convert($origine, 'pdf', $destination);
    }



    public function convert(string $origine, string $format, ?string $destination=null): string
    {
        if (!file_exists($origine)){
            throw new Exception("File \"$origine\" not reachable or writable.");
        }

        if (!$destination){
            $destination = $this->tempFileName(null, $format);
        }

        if (!is_writable(dirname($destination))) {
            throw new Exception('Impossible to create or update the document. Directory "'.dirname($destination).'" not reachable or writable');
        }

        $command = $this->getConvCommand();

        $command = str_replace(':host', $this->getHost(), $command);
        $command = str_replace(':format', $format, $command);
        $command = str_replace(':inputFile', $origine, $command);
        $command = str_replace(':outputFile', $destination, $command);
        exec($command, $output, $return);

        if (0 != $return) {
            throw new \Exception('La conversion de document en '.$format.' a échoué');
        }

        return $destination;
    }



    /**
     * @param string $fileName
     *
     * @return Document
     * @throws Exception
     */
    public function saveToFile(string $fileName): Document
    {
        $actualFile = $this->prepareSaving($fileName);

        return $this;
    }



    public function saveToPdf(string $filename)
    {
        $this->toPdf($this->getArchive()->filename, $filename);

        return $this;
    }



    /**
     * @return string
     * @throws Exception
     */
    public function saveToData(): string
    {
        $actualFile = $this->prepareSaving();

        return file_get_contents($actualFile);
    }



    /**
     * @return string
     */
    public function getTmpDir(): string
    {
        if (!$this->tmpDir) {
            return sys_get_temp_dir();
        }

        return $this->tmpDir;
    }



    /**
     * @param string $tmpDir
     *
     * @return Document
     */
    public function setTmpDir(string $tmpDir): Document
    {
        $this->tmpDir = $tmpDir;
        if (!file_exists($tmpDir)) {
            mkdir($tmpDir);
        }

        return $this;
    }



    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }



    /**
     * @param string $host
     *
     * @return Document
     */
    public function setHost(string $host)
    {
        $this->host = $host;

        return $this;
    }



    /**
     * @param null $prefix
     *
     * @return string
     */
    private function tempFileName(?string $prefix = null, string $ext = 'odt'): string
    {
        $tmpDir = $this->getTmpDir();
        if ('/' != substr($tmpDir, -1)) {
            $tmpDir .= '/';
        }

        $tempFileName     = uniqid($tmpDir . $prefix) . '.' . $ext;
        $this->tmpFiles[] = $tempFileName;

        return $tempFileName;
    }



    /**
     * @return array
     */
    public function getFormatters(): array
    {
        return $this->formatters;
    }



    /**
     * @param string $variable
     * @param mixed $format
     *
     * @return Document
     */
    public function addFormatter(string $variable, $format): Document
    {
        $this->formatters[$variable] = $format;

        return $this;
    }



    /**
     * PHP 5 introduces a destructor concept similar to that of other object-oriented languages, such as C++.
     * The destructor method will be called as soon as all references to a particular object are removed or
     * when the object is explicitly destroyed or in any order in shutdown sequence.
     *
     * Like constructors, parent destructors will not be called implicitly by the engine.
     * In order to run a parent destructor, one would have to explicitly call parent::__destruct() in the destructor body.
     *
     * Note: Destructors called during the script shutdown have HTTP headers already sent.
     * The working directory in the script shutdown phase can be different with some SAPIs (e.g. Apache).
     *
     * Note: Attempting to throw an exception from a destructor (called in the time of script termination) causes a fatal
     * error.
     *
     * @return void
     * @link https://php.net/manual/en/language.oop5.decon.php
     */
    public function __destruct()
    {
        /* On supprime les éventuels fichiers temporaires pour libérer l'espace */
        foreach ($this->tmpFiles as $tmpFile) {
            if (file_exists($tmpFile)) {
                @unlink($tmpFile);
            }
        }
    }



    /**
     * @param $fileName
     *
     * @return Document
     * @throws Exception
     */
    public function download($fileName): Document
    {
        $actualFile = $this->prepareSaving();

        if (headers_sent()) {
            throw new Exception('Headers Allready Sent');
        }

        $contentType = $this->isPdfOutput() ? 'application/pdf' : 'application/vnd.oasis.opendocument.text';

        header('Content-type: ' . $contentType);
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
        header('Content-Transfer-Encoding: binary');
        header('Pragma: no-cache');
        header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
        readfile($actualFile);

        return $this;
    }



    /**
     * @param DOMNode $node
     * @param string  $name
     *
     * @return DOMNode[]
     * @throws Exception
     */
    public function find(DOMNode $node, string $name): DOMNodeList
    {
        list($namespace, $name) = explode(':', $name);

        return $node->getElementsByTagNameNS($this->getNamespaceUrl($namespace), $name);
    }



    /**
     * @param DOMDocument $document
     * @param string      $name
     * @param array       $attrs
     *
     * @return DOMElement
     * @throws Exception
     */
    public function newElement(DOMDocument $document, string $name, array $attrs = []): DOMElement
    {
        list($namespace) = explode(':', $name);

        $newNode = $document->createElementNS($this->getNamespaceUrl($namespace), $name);
        foreach ($attrs as $attrName => $attrValue) {
            list($attrNS) = explode(':', $attrName);
            $newNode->setAttributeNS($this->getNamespaceUrl($attrNS), $attrName, $attrValue);
        }

        return $newNode;
    }
}
