<?php

namespace Unicaen\OpenDocument;

use Unicaen\OpenDocument\Calc\Sheet;

class Calc
{

    const PARAGRAPH = 'text:p';
    const TABLE_ROW = 'table:table-row';

    private \DOMNode $node;

    private Document $document;

    /**
     * @var Sheet[]
     */
    private array $sheets = [];

    private bool $parsedAliases = false;
    private array $aliases = [];



    public function __construct(Document $document)
    {
        $this->document = $document;
        $this->node = $this->document->find($this->document->getContent(), 'office:spreadsheet')[0];
    }



    public function getDocument(): Document
    {
        return $this->document;
    }



    public function getNode(): \DOMNode
    {
        return $this->node;
    }



    public function getSheet(int|string $index): Sheet
    {
        if (empty($this->sheets)) {
            $nodes = $this->document->find($this->node, 'table:table');
            foreach ($nodes as $ind => $node) {
                $sheet = new Sheet($this, $ind, $node);
                $this->sheets[$ind] = $sheet;
                $this->sheets[$sheet->getName()] = $sheet;
            }
        }

        if (!isset($this->sheets[$index])) {
            throw new \Exception('La feuille ' . $index . ' n\'existe pas');
        }

        return $this->sheets[$index];
    }



    public function getAliases(): array
    {
        if (!$this->parsedAliases) {
            $this->parsedAliases = true;
            $namedExpressions = $this->document->find($this->node, 'table:named-expressions')[0];
            if ($namedExpressions) {
                $nrs = $this->document->find($namedExpressions, 'table:named-range');
                foreach ($nrs as $nr) {
                    $alias = $nr->getAttribute('table:name');
                    $target = $nr->getAttribute('table:cell-range-address');
                    $this->aliases[$alias] = $this->analyseAliasTarget($target);
                }
            }

        }

        return $this->aliases;
    }



    public function getAliasTarget(string $alias): array
    {
        $alias = trim($alias);
        $aliases = $this->getAliases();
        if (array_key_exists($alias, $aliases)){
            return $aliases[$alias];
        }else{
            throw new \Exception('L\'alias '.$alias.' n\'a pas été trouvé');
        }
    }



    private function analyseAliasTarget(string $target): array
    {
        $target = str_replace('$', '', $target);
        $dot = strpos($target, '.');
        $sheetName = substr($target, 0, $dot);
        $target = str_replace('.', '', substr($target, $dot));

        if (str_contains($target, ':')) {
            $target = $this->analyseAliasRange($target);
        } else {
            $target = $this->analyseAliasCell($target);
        }

        $target['sheet'] = $sheetName;

        return $target;
    }



    private function analyseAliasCell(string $cell): array
    {
        return [
            'type' => 'cell',
            'name' => $cell,
        ];
    }



    private function analyseAliasRange(string $range): array
    {
        [$begin, $end] = explode(':', $range);

        $cBegin = Calc::cellNameToCoords($begin);
        $cEnd = Calc::cellNameToCoords($end);

        return [
            'type'          => 'range',
            'begin'         => $begin,
            'end'           => $end,
            'rowBegin'      => $cBegin['row'],
            'rowEnd'        => $cEnd['row'],
            'colBegin'      => $cBegin['col'],
            'colNamedBegin' => Calc::numberToLetter($cBegin['col']),
            'colEnd'        => $cEnd['col'],
            'colNamedEnd'   => Calc::numberToLetter($cEnd['col']),
        ];
    }



    public static function numberToLetter(int $number): string
    {
        $sl = floor(($number - 1) / 26);
        $tl = floor(($sl - 1) / 26);
        $ql = floor(($tl - 1) / 26);

        $letter = '';
        if ($ql > 0) $letter .= chr((($ql - 1) % 26) + 65);
        if ($tl > 0) $letter .= chr((($tl - 1) % 26) + 65);
        if ($sl > 0) $letter .= chr((($sl - 1) % 26) + 65);
        if ($number > 0) $letter .= chr((($number - 1) % 26) + 65);

        return $letter;
    }



    public static function letterToNumber(string $letter): int
    {
        $len = strlen($letter);
        $number = 0;
        for ($i = 0; $i < $len; $i++) {
            $number += (ord(substr($letter, $i, 1)) - 64) * pow(26, $len - $i - 1);
        }

        return $number;
    }



    public static function cellNameToCoords(string $name): array
    {
        $coords = [
            'absRow' => false,
            'absCol' => false,
        ];

        $col = '';
        $row = '';

        $start = 0;
        if (str_starts_with($name, '$')) {
            $start = 1;
            $coords['absCol'] = true;
        }

        for ($i = $start; $i < strlen($name); $i++) {
            $chr = substr($name, $i, 1);
            $num = ord($chr);
            if ($num > 64 && $num < 65 + 26) {
                $col .= $chr;
            } else {
                if ($chr == '$') {
                    $coords['absRow'] = true;
                } else {
                    $row .= $chr;
                }
            }
        }

        $coords['col'] = self::letterToNumber($col);
        $coords['row'] = (int)$row;

        return $coords;
    }



    public static function coordsToCellName(int $col, int $row): string
    {
        return self::numberToLetter($col) . (string)$row;
    }
}