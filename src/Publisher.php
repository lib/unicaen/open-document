<?php

namespace Unicaen\OpenDocument;

use Exception;
use DOMDocument;
use DOMElement;
use DOMNode;
use DOMNodeList;
use ZipArchive;

class Publisher
{
    const PAGE_BREAK_NAME = 'UNICAEN_PAGE_BREAK';

    const PARAGRAPH = 'text:p';
    const TABLE_ROW = 'table:table-row';

    /**
     * Lecteur de fichier OpenDocument
     *
     * @var Document
     */
    private $document;

    /**
     * Contenu XML du corps de texte
     *
     * @var DOMDocument
     */
    private $content;

    /**
     * @var DOMElement
     */
    private $body;

    /**
     * Ajoute un saut de page automatiquement entre deux instances de document lors du publipostage
     *
     * @var boolean
     */
    private $autoBreak = true;

    /**
     * @var array
     */
    private $values = [];

    /**
     * Variable contenant le résultat final du content.xml
     *
     * @var string
     */
    private $outContent;

    /**
     * @var bool
     */
    private $published = false;



    /**
     * @return Document
     */
    public function getDocument(): Document
    {
        return $this->document;
    }



    /**
     * @param Document $document
     *
     * @return Publisher
     */
    public function setDocument(Document $document): Publisher
    {
        $this->document = $document;

        return $this;
    }



    /**
     * @return DOMElement
     */
    public function getBody(): DOMElement
    {
        return $this->body;
    }



    /**
     * Peuple l'instance courante du document et l'ajoute à la suite du fichier
     *
     * @param array $values
     */
    public function add(array $values): Publisher
    {
        $this->values[] = $values;

        return $this;
    }



    /**
     * @return array
     */
    public function getValues(): array
    {
        return $this->values;
    }



    /**
     * @param array $values
     */
    public function setValues(array $values): Publisher
    {
        $this->values = $values;

        return $this;
    }



    /**
     * @return bool
     */
    public function isAutoBreak(): bool
    {
        return $this->autoBreak;
    }



    /**
     * @param bool $autoBreak
     *
     * @return Publisher
     */
    public function setAutoBreak(bool $autoBreak): Publisher
    {
        $this->autoBreak = $autoBreak;

        return $this;
    }



    /**
     * @param DOMElement $element
     *
     * @return Publisher
     * @throws Exception
     */
    public function getVariables(DOMElement $element): array
    {
        $textNs = $this->getDocument()->getNamespaceUrl('text');

        $variables = [];
        $vElements = $element->getElementsByTagNameNS($textNs, 'variable-set');
        foreach ($vElements as $vElement) {
            $name = $vElement->getAttributeNS($textNs, 'name');

            if (!isset($variables[$name])) $variables[$name] = [];
            $variables[$name][] = $vElement;
        }

        return $variables;
    }



    public function getSections(DOMElement $element): array
    {
        $textNs = $this->getDocument()->getNamespaceUrl('text');

        $sections  = [];
        $vElements = $this->findFrom($element, 'text:section');
        foreach ($vElements as $vElement) {
            $name = $vElement->getAttributeNS($textNs, 'name');

            if (!isset($sections[$name])) $sections[$name] = [];
            $sections[$name][] = $vElement;
        }

        return $sections;
    }



    public function hideSection(DOMElement $section): Publisher
    {
        $section->parentNode->removeChild($section);

        return $this;
    }



    /**
     * @param DOMElement $element
     * @param mixed      $value
     *
     * @return Publisher
     */
    protected function setVariable(DOMElement $element, $value): Publisher
    {
        $textNs = $this->getDocument()->getNamespaceUrl('text');
        $document = $element->ownerDocument;

        if (is_string($value) && false !== strpos($value, "\n")){
            $value = explode("\n", $value);
        }else{
            $value = (array)$value;
        }
        for ($i = 0; $i < count($value); $i++) {
            if ($i > 0) {
                $returnVNode = $document->createElementNS($textNs, 'text:line-break');
                $element->parentNode->insertBefore($returnVNode, $element);
            }
            $vText = $document->createTextNode($this->getDocument()->formatValue($element->nodeValue,$value[$i]));
            $element->parentNode->insertBefore($vText, $element);
        }
        $element->parentNode->removeChild($element);

        return $this;
    }



    private function addPageBreakStyle(): Publisher
    {
        /* get office:automatic-styles node */
        $styles = $this->content->getElementsByTagNameNS(
            $this->getDocument()->getNamespaceUrl('office'),
            'automatic-styles'
        )->item(0);

        $stylePageBreak           = $this->newElement('style:style', [
            'style:name'              => self::PAGE_BREAK_NAME,
            'style:family'            => 'paragraph',
            'style:parent-style-name' => 'Standard',
            'style:master-page-name'  => 'Standard',
        ]);
        $stylePageBreakProperties = $this->newElement('style:paragraph-properties', [
            'style:writing-mode' => 'page',
            'style:page-number'  => '1',
        ]);

        $stylePageBreak->appendChild($stylePageBreakProperties);
        $styles->appendChild($stylePageBreak);

        return $this;
    }



    private function addPageBreak(DOMElement $element): Publisher
    {
        $textNs = $this->getDocument()->getNamespaceUrl('text');

        $pageBreak = $this->content->createElementNS($textNs, 'text:p');
        $pageBreak->setAttributeNS($textNs, 'text:style-name', self::PAGE_BREAK_NAME);
        $element->insertBefore($pageBreak, $element->firstChild);

        return $this;
    }



    /**
     * @return Publisher
     * @throws Exception
     */
    public function publishBegin(): Publisher
    {
        /* On récupère le content du document */
        $this->content = new DOMDocument();
        $this->content->loadXML($this->getDocument()->getContent()->saveXML());

        if ($this->isAutoBreak()) {
            $this->addPageBreakStyle();
        }

        $contentText              = $this->content->saveXML();
        $officeDocumentContentPos = strpos($contentText, '<office:document-content');
        $length                   = strpos($contentText, '>', $officeDocumentContentPos) + 1;
        $this->out(substr($contentText, 0, $length));

        /* wtite all nodes, except body */
        foreach ($this->content->documentElement->childNodes as $node) {
            if ($node->nodeName != 'office:body') {
                $this->out($this->content->saveXML($node));
            }
        }

        $this->out("<office:body>");
        /* declaration tags */
        $declTags = [
            'variable-decls', 'sequence-decls', 'user-field-decls', 'dde-connexion-decls',
        ];
        foreach ($declTags as $tagName) {
            $node = $this->content->getElementsByTagNameNS($this->getDocument()->getNamespaceUrl('text'), $tagName);
            if ($node->length > 0) {
                $this->out($this->content->saveXML($node->item(0)));
                $node->item(0)->parentNode->removeChild($node->item(0));
            }
        }

        $this->body = $this->content->getElementsByTagNameNS($this->getDocument()->getNamespaceUrl('office'), 'text')[0];

        return $this;
    }



    /**
     * @return Publisher
     */
    public function publishEnd(): Publisher
    {
        $this->out("</office:body></office:document-content>");

        /* On renvoie le content dans le document */
        $this->getDocument()->getArchive()->addFromString('content.xml', $this->outContent);
        $this->published = true;

        return $this;
    }



    /**
     * @param DOMElement $element
     * @param array      $values
     *
     * @return Publisher
     * @throws Exception
     */
    public function publishValues(DOMElement $element, array $values): Publisher
    {
        if ($element === $this->body) {
            $this->getDocument()->getStylist()->setVariables($values);
        }

        $variables = $this->getVariables($element);
        $sections  = $this->getSections($element);

        foreach ($values as $name => $val) {
            if (is_array($val)) {
                /* On traite les données filles... */
                list($vname, $vparent) = explode("@", $name);
                if (isset($variables[$vname])) {
                    foreach ($variables[$vname] as $elVar) {
                        $this->publishSubData($elVar, $vparent, $val);
                    }
                }
                if (isset($sections[$vname])) {
                    foreach ($sections[$vname] as $elSec) {
                        $this->publishSubData($elSec, $vparent, $val);
                    }
                }
            } else {
                if (isset($variables[$name])) {
                    foreach ($variables[$name] as $vElement) {
                        $this->setVariable($vElement, $val ? $val : '');
                    }
                }
                if (false !== strpos($name, '@')) {
                    list($sName, $sType) = explode("@", $name);
                    if ($sType == 'text:section') {
                        $name = $sName;
                    }
                }
                if (isset($sections[$name])) {
                    foreach ($sections[$name] as $sElement) {
                        if ($val === null
                            || $val === 0
                            || $val === '0'
                            || $val === false
                            || $val === ''
                            || strtolower($val) === 'false'
                        ) {
                            $this->hideSection($sElement);
                        }
                    }
                }
            }
        }

        if ($element === $this->body) {
            $this->out($this->content->saveXML($element));
        }

        return $this;
    }



    /**
     * @param DOMElement $element
     * @param string     $parent
     * @param string     $variable
     *
     * @return DOMElement
     * @throws Exception
     */
    public function getSubDoc(DOMElement $element, string $parent, string $variable): DOMElement
    {
        $variables = $this->getVariables($element);
        if (!isset($variables[$variable][0])) {
            throw new \Exception('La variable "' . $variable . '"" n\'a pas été trouvée dans le document');
        }

        return $this->getSubDocWithVariable($variables[$variable][0], $parent);
    }



    /**
     * @param DOMElement $element
     *
     * @return Publisher
     */
    public function remove(DOMElement $element): Publisher
    {
        $element->parentNode->removeChild($element);

        return $this;
    }



    /**
     * @param DOMElement $subDoc
     * @param array      $variables
     * @param DOMElement $refNode
     *
     * @return Publisher
     */
    public function publishBefore(DOMElement $subDoc, array $variables, DOMElement $refNode): Publisher
    {
        $node = $subDoc->cloneNode(true);
        $this->publishValues($node, $variables);
        $refNode->parentNode->insertBefore($node, $refNode);

        return $this;
    }



    /**
     * @param DOMElement $element
     * @param string     $parent
     * @param DOMElement $variable
     *
     * @return DOMElement
     * @throws Exception
     */
    private function getSubDocWithVariable(DOMElement $element, string $parent): DOMElement
    {
        if ($element->tagName == 'text:section') {
            return $element; // C'est la section qui est dupliquée
        } else {
            $i     = 10;
            $found = false;
            for ($i = 0; $i < 10; $i++) {
                $parentNode = isset($parentNode) ? $parentNode->parentNode : $element->parentNode;
                if ($parentNode->nodeName == $parent) {
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                throw new \Exception('Le noeud parent de type ' . $parent . ' n\'a pas été trouvé');
            }

            return $parentNode;
        }
    }



    /**
     * @param DOMElement $element
     * @param string     $parent
     * @param array      $values
     *
     * @return Publisher
     */
    private function publishSubData(DOMElement $element, string $parent, array $values): Publisher
    {
        $parentNode = $this->getSubDocWithVariable($element, $parent);

        foreach ($values as $vals) {
            $clone = $parentNode->cloneNode(true);
            $this->publishValues($clone, $vals);
            $parentNode->parentNode->insertBefore($clone, $parentNode);
        }

        return $this->remove($parentNode);
    }



    /**
     * Construit le fichier final à partir des données
     */
    public function publish()
    {
        $this->publishBegin();

        $first = true;
        foreach ($this->values as $values) {
            $bodyNode = $this->body->cloneNode(true);
            if ($first) {
                $first = false;
            } elseif ($this->isAutoBreak()) {
                $this->addPageBreak($bodyNode);
            }

            $this->publishValues($bodyNode, $values);
            $this->getDocument()->getStylist()->setVariables($values);
            $this->out($this->content->saveXML($bodyNode));
            $first = false;
        }

        $this->publishEnd();
    }



    /**
     * @param string $name
     *
     * @return DOMNode[]
     * @throws Exception
     */
    private function find(string $name): DOMNodeList
    {
        $document = $this->getDocument();

        return $document->find($this->content, $name);
    }



    /**
     * @param DOMNode $node
     * @param         $name
     *
     * @return DOMNodeList
     * @throws Exception
     */
    private function findFrom(DOMNode $node, $name): DOMNodeList
    {
        return $this->getDocument()->find($node, $name);
    }



    /**
     * @param string $name
     * @param array  $attrs
     *
     * @return DOMElement
     */
    private function newElement(string $name, array $attrs = []): DOMElement
    {
        $document = $this->getDocument();

        return $document->newElement($this->content, $name, $attrs);
    }



    /**
     * Ajoute du contenu au fichier content.xml
     * Méthode à ne pas exploiter
     *
     * @param string $xml
     */
    public function out($xml)
    {
        $this->outContent .= $xml;
    }



    /**
     * @return string
     */
    public function getOutContent(): string
    {
        return $this->outContent;
    }



    /**
     * @return bool
     */
    public function isPublished(): bool
    {
        return $this->published;
    }



    /**
     * @param bool $published
     *
     * @return Publisher
     */
    public function setPublished(bool $published): Publisher
    {
        $this->published = $published;

        return $this;
    }

}