<?php

namespace Unicaen\OpenDocument;

use Exception;
use DOMDocument;
use ZipArchive;

/**
 * Class Data
 * @todo à terminer : non fonctionnel
 * @package Unicaen\OpenDocument
 */
class Data
{
    const PERIMETRE_TAB_LIGNE = 'table:table-row';
    const PERIMETRE_FRAME     = 'draw:frame';
    const PERIMETRE_LIST_ITEM = 'text:list-item';

    /**
     * @var string[]
     */
    public $variables = [];

    /**
     * @var
     */
    public $subDataVariable;

    /**
     * @var
     */
    public $subDataPerimetre;

    /**
     * @var Data[]
     */
    public $subData = [];



    public static function create(array $variables = [])
    {
        $data            = new self;
        $data->variables = $variables;

        return $data;
    }



    public function addSubData(string $variable, string $perimetre, array $variables = [])
    {
        $subData                   = new self;
        $subData->subDataVariable  = $variable;
        $subData->subDataPerimetre = $perimetre;
        $subData->variables        = $variables;

        return $subData;
    }
}