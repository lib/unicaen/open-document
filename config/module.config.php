<?php

namespace Unicaen\OpenDocument;

return [

    'view_manager' => [
        'template_path_stack' => [
            'import' => __DIR__ . '/../view',
        ],
    ],

];