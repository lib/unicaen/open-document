CHANGELOG
=========

6.0.10 (19/17/2024)
-------------------

- [Fix] Gestion correcte des contenus multilignes dans les cellules des tableurs

6.0.9 (05/07/2024)
------------------

- [Fix] Interpréteur de formules Calc : of:== pris en compte comme un of:=

6.0.8 (04/07/2024)
------------------

- [Fix] Détection des espaces et suppression des espaces par trim dans les variables

6.0.7 (24/05/2024)
------------------

- [Fix] détection des noms de feuilles dans les références aux cellules d'autres pages de feuilles de calcul

6.0.6
------------------

- Refactoring
- Gestion des nommages de cellules

6.0.5
------------------
- Gestion des plages en plus des cellules pour les dépendances d'expressions


6.0.4
------------------
- Améliorations : création de Display pour faciliter le debug & centralisation de toutes les conversions HTML dedans

6.0.3
------------------
- [FIX] Correction de détection des numéros de lignes
- [FIX] Remontée de NULL si valeur vide dans une cellule pour getValue au lieu de 0
- Amélioration : possibilité de fournir des coordonnées de colonnes sous forme de lettre directement

6.0.2
------------------

- Prise en compte des pourcentages

6.0.1
------------------

- Pas de reparsing d'une fonction déjà analysée

6.0.0
------------------

- Passage à PHP8


4.0
------------------

- Passage de Zend à Laminas


3.0.2
------------------

- Meilleur contrôle d'écriture des fichiers et de chargement des données

3.0.1
------------------

- Unoconv : pouvoir utiliser un serveur dédié

